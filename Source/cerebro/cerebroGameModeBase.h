// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameModeBase.h"
#include "Transaction.h"
#include "cerebroGameModeBase.generated.h"



/**
 * 
 */
UCLASS()
class CEREBRO_API AcerebroGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
		//static AcerebroGameModeBase* Default;

public:
	UPROPERTY(EditAnywhere, Category = cpp)
		int Min = 15000;

	UPROPERTY(EditAnywhere, Category = cpp)
		int Max = 30000;

	UPROPERTY(EditAnywhere, Category = cpp)
		int Count = 1000;
	
public:
	UPROPERTY(EditAnywhere, Category = cpp)
	TSubclassOf<ATransaction> Blueprint;

	//UFUNCTION(BlueprintCallable, Category = Game)
	virtual void StartPlay();

};
