// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "address.h"
#include "cerebroGameModeBase.h"
#include "Transaction.h"
#include "Miner.generated.h"



UCLASS()
class CEREBRO_API AMiner : public AActor
{
	GENERATED_BODY()

private:
	UPROPERTY(EditAnywhere, Category = cpp)
	TSubclassOf<ATransaction> Blueprint;

	void EventMined();

	
	
public:	
	// Sets default values for this actor's properties
	AMiner();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	TArray<ATransaction*> activeTxIds;
	
};
