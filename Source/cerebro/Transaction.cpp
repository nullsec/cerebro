// Fill out your copyright notice in the Description page of Project Settings.

#include "cerebro.h"
#include "Transaction.h"


TMap<UINT64, Aaddress*> ATransaction::_addressPool;

// Sets default values
ATransaction::ATransaction()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;	

	id = rand();

	beamEmitter = nullptr;
	fromMesh = nullptr;
	toMesh = nullptr;
	
	//RootComponent = transactionScene = CreateDefaultSubobject<USceneComponent>(*FString::Printf(TEXT("tx scene %llu"), id));
}

void ATransaction::SetId(UINT64 id)
{
	this->rowid = id;
}


Aaddress* ATransaction::GetRandomAddress()
{
	Aaddress* ax = nullptr;
	Aaddress** axx = nullptr;
	int id = rand() % 500;

	//previous address used
	if ((axx = _addressPool.Find(id)) != nullptr)
		return *axx;

	ax = GetWorld()->SpawnActor<Aaddress>(Blueprint, FVector((rand() % domain) - (domain >> 2), (rand() % domain) - (domain >> 2), (rand() % domain) - (domain >> 2)), FRotator::ZeroRotator);
	ax->id = id;
	
	if(_addressPool.Contains(ax->id))
	{
		UINT64 id = ax->id;
		ax->Destroy();
		/*if (GEngine)
			GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, *FString::Printf(TEXT("C=%llu"),id));*/
		return _addressPool[id];
	}
	else
	{
		_addressPool.Add(ax->id, ax);
		/*if (GEngine)
			GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, *FString::Printf(TEXT("N=%llu"), ax->id));*/
	}
		

	return ax;
}

// Called when the game starts or when spawned
void ATransaction::BeginPlay()
{
	Super::BeginPlay();

	from_address = GetRandomAddress();
	to_address = GetRandomAddress();

	beamEmitter = GetWorld()->SpawnActor<AEmitter>(Particle, FVector(0.0), FRotator::ZeroRotator);
	//_beamEmitter->SetActorParameter(TEXT("Source"), from_address);
	//_beamEmitter->SetActorParameter(TEXT("Target"), to_address);

	//beamEmitter->GetParticleSystemComponent()->SetBeamSourcePoint(0, from_address->GetActorLocation(), 0);
	//beamEmitter->GetParticleSystemComponent()->SetBeamTargetPoint(0, to_address->GetActorLocation(), 0);
	

	fromMesh = from_address->addressMeshComponent;
	toMesh = to_address->addressMeshComponent;
	
}

// Called every frame
void ATransaction::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
	if(beamEmitter != nullptr && fromMesh != nullptr)
		beamEmitter->GetParticleSystemComponent()->SetBeamSourcePoint(0, fromMesh->RelativeLocation, 0);
	else
	{
		
	}

	if(beamEmitter != nullptr && toMesh != nullptr)
		beamEmitter->GetParticleSystemComponent()->SetBeamTargetPoint(0, toMesh->RelativeLocation, 0);


}

