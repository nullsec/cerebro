// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "address.generated.h"

UCLASS()
class CEREBRO_API Aaddress : public AActor
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, Category = cpp)
	UINT64 id;

	UPROPERTY(EditAnywhere, Category=cpp)
	TMap<int, Aaddress*> _neighbours;

	UPROPERTY(EditAnywhere, Category = cpp)
	UStaticMeshComponent* addressMeshComponent;

	UPROPERTY(EditAnywhere, Category = cpp)
	USceneComponent* AddressScene;

	
public:	
	// Sets default values for this actor's properties
	Aaddress();

	//void SetId(UINT64 id);

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	
	
};
