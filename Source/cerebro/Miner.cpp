// Fill out your copyright notice in the Description page of Project Settings.

#include "cerebro.h"
#include "Miner.h"
#include "Transaction.h"
//#include "EditorModes.h"


// Sets default values
AMiner::AMiner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
}

// Called when the game starts or when spawned
FTimerHandle timer;
void AMiner::BeginPlay()
{
	Super::BeginPlay();
	
	//FTimerHandle timer;

	GetWorld()->GetTimerManager().SetTimer(timer,this, &AMiner::EventMined, 1, true);
	
}

void AMiner::EventMined()
{
	static int txId = 0;

	for(int i = 0; i < rand() % 25; i++)
	{
		auto tx = GetWorld()->SpawnActor<ATransaction>(Blueprint, FVector(rand() % 10000, rand() % 10000, rand() % 10000), FRotator::ZeroRotator);
		if( tx != NULL)
		{
			//activeTxIds.Add(tx);
			//tx->SetId(txId++);
		}			
	}
}

// Called every frame

void AMiner::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );	
}

