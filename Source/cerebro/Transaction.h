// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "address.h"

#include "Transaction.generated.h"


UCLASS()
class CEREBRO_API ATransaction : public AActor
{
	GENERATED_BODY()

private:	
	Aaddress* GetRandomAddress();

	
	
public:
	UPROPERTY(VisibleAnywhere, Category = cpp)
	UINT64 rowid;

	UPROPERTY(VisibleAnywhere, Category = cpp)
	UINT64 id;

	UPROPERTY(EditAnywhere, Category = cpp)
	TSubclassOf<Aaddress> Blueprint;

	UPROPERTY(EditAnywhere, Category = cpp)
	TSubclassOf<AEmitter> Particle;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = cpp)
	Aaddress* from_address;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = cpp)
	Aaddress* to_address;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = cpp)
	UStaticMeshComponent* fromMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = cpp)
	UStaticMeshComponent* toMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = cpp)
	AEmitter* beamEmitter;

	//UPROPERTY(EditAnywhere, Category = cpp)
	static TMap<UINT64, Aaddress*> _addressPool;

	UPROPERTY(EditAnywhere, Category = cpp)
	int domain = 10000;

	UPROPERTY(Category = a1, EditAnywhere)
	USceneComponent* transactionScene;
public:	
	// Sets default values for this actor's properties
	ATransaction();

	void SetId(UINT64 id);

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	
	
};
