// Fill out your copyright notice in the Description page of Project Settings.

#include "cerebro.h"
#include <time.h>
#include "address.h"


// Sets default values
Aaddress::Aaddress()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	const int addrPoolSize = 5000;
	srand(time(NULL));
	id = rand() % addrPoolSize;
	

	AddressScene = CreateDefaultSubobject<USceneComponent>(TEXT("Addr scene"));

	RootComponent = AddressScene;
	//RootComponent = addressMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Addr scene"));

	addressMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Addr Mesh"));
	addressMeshComponent->SetupAttachment(RootComponent);
	
	
}

// Called when the game starts or when spawned
void Aaddress::BeginPlay()
{
	Super::BeginPlay();

	//SetActorLocation(FVector(rand() % 5000, rand() % 5000, rand() % 5000));
}

// Called every frame
void Aaddress::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
}

