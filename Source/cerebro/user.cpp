// Fill out your copyright notice in the Description page of Project Settings.

#include "cerebro.h"
#include "user.h"


// Sets default values
Auser::Auser()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void Auser::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void Auser::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

// Called to bind functionality to input
void Auser::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

